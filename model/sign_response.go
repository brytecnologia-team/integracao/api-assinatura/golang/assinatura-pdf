package model

import (
	"strconv"
	"strings"
)

type Content struct {
	Nonce string
	MessageDigest string
}

type ResponseInitialization struct {
	Nonce string
	AssinaturasInicializadas []Content
}

type ResponseFinalization struct {
	Quantity int `json:"quantidadeAssinaturas"`
	Identifier string `json:"identificador"`
	Documents []Document `json:"documentos"`
}

type Document struct {
	Hash string `json:"hash"`
	Nonce string `json:"nonce"`
	Links []struct{
		Href string `json:"href"`
	} `json:"links"`
}

func (doc Document) Report() string {
	sb := strings.Builder{}
	sb.WriteString("\t\tDocument {\n")
	sb.WriteString("\t\t\tHash: " + doc.Hash + "\n")
	sb.WriteString("\t\t\tNonce: " + doc.Nonce + "\n")
	if len(doc.Links) > 0 {
		sb.WriteString("\t\t\tReference: " + doc.Links[0].Href + "\n")
	}
	sb.WriteString("\t\t}\n")
	return sb.String()
}

func (rf ResponseFinalization) Report() string {
	sb := strings.Builder{}
	sb.WriteString("Response Finalization {\n")
	sb.WriteString("\tQuantidade de assinaturas:" + strconv.Itoa(rf.Quantity) + "\n")
	sb.WriteString("\tIdentificador: " + rf.Identifier + "\n")
	sb.WriteString("\tDocumentos : [\n")
	for _, doc := range rf.Documents {
		sb.WriteString(doc.Report())
	}
	sb.WriteString("\t]\n")
	sb.WriteString("}\n")
	return sb.String()
}


