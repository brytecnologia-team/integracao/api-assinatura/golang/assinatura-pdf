package model

import (
	"assinatura-pdf/config"
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/base64"
	"golang.org/x/crypto/pkcs12"
	"io/ioutil"
	"log"
	"os"
)

type BryCertificate struct {
	Content []byte
	Cert *x509.Certificate
	PublicKey rsa.PublicKey
	PrivateKey rsa.PrivateKey
}

//load certificate from file .p12
func LoadCertificate() BryCertificate {
	file, err := os.Open(config.PrivateKeyLocation)
	if err != nil {
		log.Fatal(err)
	}
	certBytes, err := ioutil.ReadAll(file)
	if err != nil {
		log.Fatal(err)
	}
	privateKey, certificate, err := pkcs12.Decode(certBytes, config.PrivateKeyPassword)
	if err != nil {
		log.Fatal(err)
	}

	pk := privateKey.(*rsa.PrivateKey)

	return BryCertificate{
		Content: certBytes,
		Cert:    certificate,
		PublicKey:  pk.PublicKey,
		PrivateKey: *pk,
	}
}

func (cert BryCertificate) Sign(data []byte) string {
	var err error
	var signature []byte
	switch config.HashAlgorithm {
	case config.Sha1:
		h := crypto.SHA1.New()
		h.Write(data)
		signature, err = rsa.SignPKCS1v15(rand.Reader, &cert.PrivateKey, crypto.SHA1, h.Sum(nil))
	case config.Sha256:
		h := crypto.SHA256.New()
		h.Write(data)
		signature, err = rsa.SignPKCS1v15(rand.Reader, &cert.PrivateKey, crypto.SHA256, h.Sum(nil))
	case config.Sha512:
		h := crypto.SHA512.New()
		h.Write(data)
		signature, err = rsa.SignPKCS1v15(rand.Reader, &cert.PrivateKey, crypto.SHA512, h.Sum(nil))
	}

	if err != nil {
		log.Fatal(err)
	}
	return base64.StdEncoding.EncodeToString(signature)
}

func (cert BryCertificate) GetX509Base64() string {
	return base64.StdEncoding.EncodeToString(cert.Cert.Raw)
}