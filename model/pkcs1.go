package model

type PKCS1Dto struct {
	Nonce string `json:"nonce"`
	MessageDigest string `json:"messageDigest"`
	SignatureValue string `json:"cifrado"`
}
