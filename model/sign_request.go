package model

//inicializarAssinaturaPkcs1
type InitializationData struct {
	Nonces []string `json:"nonces"`
	Perfil string `json:"perfil"`
	Certificado string `json:"certificado"`
	AlgoritmoHash string `json:"algoritmoHash"`
	FormatoDadosSaida string `json:"formatoDadosSaida"`
	FormatoDadosEntrada string `json:"formatoDadosEntrada"`
}

type RequestFinalization struct {
	Nonce string `json:"nonce"`
	AssinaturasPkcs1 []PKCS1Dto `json:"assinaturasPkcs1"`
	FormatoDeDados string `json:"formatoDeDados"`
}
