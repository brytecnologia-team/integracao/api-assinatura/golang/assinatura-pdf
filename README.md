# Geração de Assinatura PDF com certificado em arquivo

Código de exemplo para geração de assinatura PDF utilizando chave e certificado armazenados em arquivo (PKCS12).

  - Passo 1: Carregamento da chave privada e conteúdo do certificado digital.
  - Passo 2: Inicialização da assinatura e produção dos artefatos signedAttributes.
  - Passo 3: Cifragem dos dados inicializados com o certificado armazenado em disco.
  - Passo 4: Finalização da assinatura e obtenção do artefato assinado.

### Tech

O exemplo utiliza das bibliotecas Golang abaixo:
* [Golang Version] - 1.12.9
* [Package golang.org/x/crypto/pkcs12] - Package pkcs12 implements some of PKCS#12.
* [Package crypto] - Package crypto collects common cryptographic constants.
* [Package net/http] - Package http provides HTTP client and server implementations.
* [Package io/ioutil] - Package ioutil implements some I/O utility functions
* [Package mime/multipart] - Package multipart implements MIME multipart parsing, as defined in RFC 2046.

### Variáveis que devem ser configuradas

O exemplo por consumir a API de assinatura necessita ser configurado com token de acesso válido.

Esse token de acesso pode ser obtido através da documentação disponibilizada no [Docs da API de Assinatura](https://api-assinatura.bry.com.br) ou através da conta de usuário no [BRy Cloud](https://cloud.bry.com.br/home/usuarios/autenticado/aplicacoes).

Caso ainda não esteja cadastrado, [cadastra-se](https://www.bry.com.br/) para ter acesso a nossa plataforma de serviços.

Além disso, no processo de geração de assinatura é obrigatório a posse de um certificado digital que identifica o autor do artefato assinado que será produzido.

Por esse motivo, é necessário configurar a localização deste arquivo no computador, bem como a senha para acessar seu conteúdo.

**Observação**

Por se tratar de uma informação sensível do usuário, reforçamos que a informação inserida no exemplo é utilizada pontualmente para a produção da assinatura.

| Variável | Descrição | Arquivo de Configuração |
| ------ | ------ | ------ |
| <ACCESS_TOKEN> | Access Token para o consumo do serviço (JWT). | sign_config.go
| <LOCATION_OF_PRIVATE_KEY_FILE> | Localização do arquivo da chave privada a ser configurada na aplicação. | sign_config.go
| <PRIVATE_KEY_PASSWORD> | Senha do arquivo da chave privada a ser configurada na aplicação. | sign_config.go

## Adquirir um certificado digital

É muito comum no início da integração não se conhecer os elementos mínimos necessários para consumo dos serviços.

Para assinar digitalmente um documento, é necessário, antes de tudo, possuir um certificado digital, que é a identidade eletrônica de uma pessoa ou empresa.

O certificado, na prática, consiste em um arquivo contendo os dados referentes à pessoa ou empresa, protegidos por criptografia altamente complexa e com prazo de validade pré-determinado.

Os elementos que protegem as informações do arquivo são duas chaves de criptografia, uma pública e a outra privada. Sendo estes elementos obrigatórios para a execução deste exemplo.

**Entendido isso, como faço para obter meu certificado digital?**

[Obtenha agora](https://certificado.bry.com.br/certificate-issue-selection) um Certificado Digital Corporativo de baixo custo para testes de integração.

Entenda mais sobre o [Certificado Corporativo](https://www.bry.com.br/blog/certificado-digital-corporativo/).  

### Uso

Para execução da aplicação de exemplo:
```
go run main.go
```

[Golang Version]: <https://golang.org/dl/>
[Package golang.org/x/crypto/pkcs12]: <https://godoc.org/golang.org/x/crypto/pkcs12>
[Package crypto]: <https://golang.org/pkg/crypto/>
[Package net/http]: <https://golang.org/pkg/net/http/>
[Package io/ioutil]: <https://golang.org/pkg/io/ioutil/>
[Package mime/multipart]: <https://golang.org/pkg/mime/multipart/>

