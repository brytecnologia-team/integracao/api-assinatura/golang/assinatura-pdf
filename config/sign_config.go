package config

const(
	AccessToken = "<ACCESS_TOKEN>"
)

const (
	PrivateKeyLocation = "<LOCATION_OF_PRIVATE_KEY_FILE>"
	PrivateKeyPassword = "<PRIVATE_KEY_PASSWORD>"
)

const(
	UrlInitializeSignature = "https://hub2.bry.com.br/fw/v1/pdf/pkcs1/assinaturas/acoes/inicializar"
	UrlFinalizeSignature = "https://hub2.bry.com.br/fw/v1/pdf/pkcs1/assinaturas/acoes/finalizar"
)

// Available values: "BASIC", "CHAIN", "CHAIN_CRL", "TIMESTAMP", "COMPLETE", "ADRB", "ADRT", "ADRV", "ADRC", "ADRA", "ETSI_B", "ETSI_T", "ETSI_LT" and "ETSI_LTA".
const(
	Basic = "BASIC"
	Chain = "CHAIN"
	ChainCrl = "CHAIN_CRL"
	Timestamp = "TIMESTAMP"
	Complete = "COMPLETE"
	Adrb = "ADRB"
	Adrt = "ADRT"
	Adrv = "ADRV"
	Adrc = "ADRC"
	Adra = "ADRA"
	EtsiB = "ETSI_B"
	EtsiT = "ETSI_T"
	EtsiLT = "ETSI_LT"
	EtsiLTA = "ETSI_LTA"
)

// Available values: "SHA1", "SHA256" e "SHA512".
const(
	Sha1 = "SHA1"
	Sha256 = "SHA256"
	Sha512 = "SHA512"
)

//Input and output format values:
const(
	Base64 = "Base64"
)

// Available values:
//ENVELOPED = signature is contained in the content of the document,
//ENVELOPING = signature contains the content of the document
//DETACHED = signature and document content are separate
const(
	Enveloped = "ENVELOPED"
	Enveloping = "ENVELOPING"
	Detached = "DETACHED"
)

// Available values: "SIGNATURE", "CO_SIGNATURE	" and "COUNTER_SIGNATURE".
const(
	Signature = "SIGNATURE"
	CoSignature = "CO_SIGNATURE"
	CounterSignature = "COUNTER_SIGNATURE"
)

//Request attributes
const(
	Nonce = "1" //Request identifier
	Profile = Basic
	HashAlgorithm = Sha256
	OperationType = Signature
	NonceOfDocument = "1" // Identifier of the original document within a batch
	DocumentPath = "./resource/document.pdf" //location where the original document is stored
)
