package mapper

import (
	"assinatura-pdf/model"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
)

type JsonMapper struct {}

func (jsonMapper JsonMapper) DecodeToString(response *http.Response) (string, error){
	bytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return "", err
	}
	return string(bytes), err
}

func (jsonMapper JsonMapper) DecodeToResponseInitialization(response *http.Response) (*model.ResponseInitialization, error) {
	bytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}
	log.Print("Response Initialization : " + string(bytes))
	var ri model.ResponseInitialization
	err = json.Unmarshal(bytes, &ri)
	return &ri, err
}

func (jsonMapper JsonMapper) DecodeToResponseFinalization(response *http.Response) (*model.ResponseFinalization, error) {
	bytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Response finalization: " + string(bytes))
	var rf model.ResponseFinalization
	err = json.Unmarshal(bytes, &rf)
	return &rf, err
}
