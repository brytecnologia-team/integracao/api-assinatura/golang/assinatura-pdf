package mapper

import (
	"assinatura-pdf/model"

)

type PKCS1Mapper struct {}

func (pkcs1Mapper PKCS1Mapper) DecodeToPKCS1(ri model.ResponseInitialization) ([]model.PKCS1Dto, error){
	data := []model.PKCS1Dto{}
	for _, signature := range ri.AssinaturasInicializadas {
		data = append(data, model.PKCS1Dto{Nonce: signature.Nonce, MessageDigest: signature.MessageDigest})
	}
	return data, nil
}
