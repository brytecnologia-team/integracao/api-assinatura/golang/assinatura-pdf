package sign

import (
	"assinatura-pdf/config"
	"assinatura-pdf/mapper"
	"assinatura-pdf/model"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

func Initialize(cert model.BryCertificate) *http.Response{
	data := model.InitializationData{
		Nonces:              []string{config.NonceOfDocument},
		Perfil:              config.Profile,
		Certificado:         cert.GetX509Base64(),
		AlgoritmoHash:       config.Sha256,
		FormatoDadosSaida:   config.Base64,
		FormatoDadosEntrada: config.Base64,
	}
	bytes, err := json.Marshal(data)
	if err != nil {
		log.Fatal(bytes)
	}
	log.Println(string(bytes))
	params := map[string]string {"dados_inicializar" : string(bytes) }
	files := map[string]string {"documento": config.DocumentPath }

	req, err := NewFormDataRequest(config.UrlInitializeSignature, params, files)
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", config.AccessToken))

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	if resp.StatusCode != 200 {
		str, err := mapper.JsonMapper{}.DecodeToString(resp)
		if err != nil {
			log.Fatal(err)
		}
		log.Fatal(str)
	}
	return resp
}

func GetContentInitializationResponse(response *http.Response) (string, []model.PKCS1Dto) {
	ri, err := mapper.JsonMapper{}.DecodeToResponseInitialization(response)
	if err != nil {
		log.Fatal(err)
	}
	data, err := mapper.PKCS1Mapper{}.DecodeToPKCS1(*ri)
	if err != nil {
		log.Fatal(err)
	}
	return ri.Nonce, data
}

func EncryptSignedAttributes(attributes []model.PKCS1Dto, cert model.BryCertificate) {
	if len(attributes) > 0 {
		content, err := base64.StdEncoding.DecodeString(attributes[0].MessageDigest)
		if err != nil {
			log.Fatal(err)
		}
		attributes[0].SignatureValue = cert.Sign(content)
	}
}

func Finalization(nonce string, attributes []model.PKCS1Dto){
	rf := model.RequestFinalization{
		Nonce:            nonce,
		AssinaturasPkcs1: attributes,
		FormatoDeDados:   config.Base64,
	}
	req, err := NewJsonRequest(config.UrlFinalizeSignature, rf)
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", config.AccessToken))

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	if resp.StatusCode != 200 {
		str, err := mapper.JsonMapper{}.DecodeToString(resp)
		if err != nil {
			log.Fatal(err)
		}
		log.Fatal(str)
	}
	respf, err := mapper.JsonMapper{}.DecodeToResponseFinalization(resp)
	if err != nil {
		log.Fatal(err)
	}
	log.Println(respf.Report())
}
